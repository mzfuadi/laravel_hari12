<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FORM</title>
</head>
<body>
    <form action="{{ route('welcome') }}" method="POST">
     {{ csrf_field() }}
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <label>First Name:</label> <br />
    <input type="text" name="firstname" /> <br />
    <br />
    <label>Last Name:</label> <br />
    <input type="text" name="lastname" /> <br />
    <br />
    <label>Gender:</label> <br />
    <input type="radio" name="jk" />Male<br />
    <input type="radio" name="jk" />Female<br />
    <input type="radio" name="jk" />Other<br />
    <br />
    <label>Nasionality:</label><br />
    <br />
    <select>
        <option>Indonesian</option>
        <option>Malaysian</option>
        <option>Singapore</option>
    </select><br />
    <br />
    <label>Language Spoken :</label><br />
    <input type="radio" name="jk" />Bahasa Indonesia<br />
    <input type="radio" name="jk" />English<br />
    <input type="radio" name="jk" />Other<br />
    <br />
    <label>Bio:</label><br />
    <textarea cols="35" rows="10"></textarea><br />
    <input type="submit" value="Sign Up" /><br />
</form>
</body>
</html>